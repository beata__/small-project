package com.codementor.authentication;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;


@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TokenDto {

    private final String validation;
    private final String jwt;
    @JsonProperty("refresh_token")
    private final String refreshToken;
}
