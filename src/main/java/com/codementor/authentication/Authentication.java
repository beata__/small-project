package com.codementor.authentication;

import com.codementor.authentication.token.TokenService;
import com.codementor.response.HeaderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@Slf4j
@RequestMapping("/access-tokens")
@RequiredArgsConstructor
public class Authentication {

    private final TokenService tokenService;
    private final HeaderService headerService;
    private final UserRepository repository;


    @RequestMapping(method = POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TokenDto> login(RequestEntity<UserDto> loginDto) {
        long start = System.nanoTime();
        UserDto body = loginDto.getBody();
        String email = body.getEmail();
        String password = body.getPassword();
        if (StringUtils.isEmpty(email) || StringUtils.isEmpty(password)) {
            return generateUnauthorizedResponse(loginDto, start, "Password or Email Are missing");
        }
        UserDto one = repository.findOne(email);
        if (one == null || !one.getPassword().equals(password)) {
            return generateUnauthorizedResponse(loginDto, start, "Wrong Password or Email");
        }
        TokenDto tokenDto = tokenService.buildToken(one);
        storeRefreshToken(one, tokenDto.getRefreshToken());
        return headerService.maxAgeZeroCache(loginDto, start, ResponseEntity.ok()).body(tokenDto);

    }

    private void storeRefreshToken(UserDto one, String refreshToken) {
        one.setRefreshToken(refreshToken);
        repository.saveAndFlush(one);
    }

    private ResponseEntity<TokenDto> generateUnauthorizedResponse(RequestEntity<UserDto> loginDto, long start, String message) {
        return headerService.maxAgeZeroCache(loginDto, start, ResponseEntity.status(HttpStatus.UNAUTHORIZED))
                            .body(TokenDto.builder()
                                          .validation(message)
                                          .build());
    }

    @RequestMapping(path = "/refresh", method = POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TokenDto> refreshToken(RequestEntity<UserDto> userDtoRequestEntity) {
        long start = System.nanoTime();

        String refreshToken = userDtoRequestEntity.getBody().getRefreshToken();
        TokenDto token = tokenService.refreshToken(refreshToken);
        if (token.getValidation() != null) {
            return headerService.maxAgeZeroCache(userDtoRequestEntity, start, ResponseEntity.status(HttpStatus.UNAUTHORIZED))
                                .body(token);
        }
        return headerService.maxAgeZeroCache(userDtoRequestEntity, start, ResponseEntity.ok()).body(token);
    }

    @RequestMapping(method = DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> logout(RequestEntity<UserDto> refreshTokenDto) {
        long start = System.nanoTime();
        String refreshToken = refreshTokenDto.getBody().getRefreshToken();
        String email = tokenService.extractEmail(refreshToken);
        removeRefreshToken(email);
        return headerService.noCache(refreshTokenDto, start, ResponseEntity.ok())
                            .build();
    }

    private void removeRefreshToken(String email) {
        UserDto one = repository.findOne(email);
        if (one != null) {
            storeRefreshToken(one, null);
        }
    }


}



