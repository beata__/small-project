package com.codementor.authentication;

import java.util.Optional;

import com.codementor.authentication.token.TokenService;
import com.codementor.response.HeaderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/me")
public class CurrentUser {

    private final UserRepository users;
    private final TokenService tokenService;
    private final HeaderService headerService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<UserDto> getMe(RequestEntity<Void> requestEntity) {
        long start = System.nanoTime();
        Optional<String> token = headerService.extractToken(requestEntity);
        if (!token.isPresent() || !tokenService.isTokenValid(token)) {
            return headerService.maxAgeZeroCache(requestEntity, start, ResponseEntity.status(HttpStatus.UNAUTHORIZED)).build();
        }
        UserDto one = cleanUpUser(token.get());
        return headerService.maxAgeZeroCache(requestEntity, start, ResponseEntity.ok()).body(one);
    }

    private UserDto cleanUpUser(String token) {
        UserDto one = users.findOne(tokenService.extractEmail(token));
        one.setPassword(null);
        one.setRefreshToken(null);
        return one;
    }
}
