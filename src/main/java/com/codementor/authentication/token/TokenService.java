package com.codementor.authentication.token;

import java.sql.Date;
import java.time.Duration;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import javax.crypto.SecretKey;

import com.codementor.authentication.TokenDto;
import com.codementor.authentication.UserDto;
import com.codementor.authentication.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TokenService {

    private static final String EMAIL = "email";
    private final SecretKey secretKey;
    private final UserRepository repository;

    public TokenDto buildToken(UserDto body) {

        String email = body.getEmail();
        String refreshToken = generateRefreshToken(email);
        String accessToken = generateAccessToken(email, refreshToken);
        return TokenDto.builder()
                       .refreshToken(refreshToken)
                       .jwt(accessToken)
                       .build();
    }

    public TokenDto refreshToken(String refreshToken) {
        try {
            String userEmail = validateRefreshToken(refreshToken);
            return TokenDto.builder().jwt(generateAccessToken(userEmail, refreshToken)).build();
        } catch (Exception exception) {
            return TokenDto.builder().validation("Log in again").build();
        }
    }

    public boolean isTokenValid(Optional<String> accessToken) {
        if (!accessToken.isPresent()) return false;
        try {
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(accessToken.get());
            String token = claimsJws.getBody().get("token", String.class);
            String refreshEmail = validateRefreshToken(token);

            return Objects.equals(refreshEmail, extractEmail(accessToken.get()));
        } catch (Exception e) {
            return false;
        }
    }

    public String extractEmail(String token) {
        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token);
        return claimsJws.getBody().get(EMAIL, String.class);
    }

    private String generateRefreshToken(String email) {
        return Jwts.builder().signWith(SignatureAlgorithm.HS512, secretKey).claim(EMAIL, email).compact();
    }

    private String generateAccessToken(String email, String refreshToken) {
        return Jwts.builder().signWith(SignatureAlgorithm.HS512, secretKey)
                   .setExpiration(Date.from(Instant.now().plus(Duration.ofMinutes(10)))).claim(EMAIL, email)
                   .claim("token", refreshToken).compact();
    }

    private String validateRefreshToken(String refreshToken) {

        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(refreshToken);

        String email = claimsJws.getBody().get(EMAIL, String.class);
        UserDto one = repository.findOne(email);
        Objects.requireNonNull(one, "User does not exist");
        if (refreshToken.equals(one.getRefreshToken())) {
            return email;
        }
        throw new IllegalArgumentException("Refresh token does not match stored one");


    }
}
