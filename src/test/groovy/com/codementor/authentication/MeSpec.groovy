package com.codementor.authentication

import com.codementor.authentication.token.TokenService
import com.codementor.response.HeaderService
import org.springframework.http.RequestEntity
import org.springframework.http.ResponseEntity
import spock.lang.Specification

class MeSpec extends Specification {
    UserRepository repository = Mock(UserRepository)
    HeaderService headerService = Spy(HeaderService)
    TokenService tokenService = Mock(TokenService)
    CurrentUser users = new CurrentUser(repository,tokenService, headerService)

    def "should return user if token is valid"() {
        given:
            Optional token =Optional.of('token')
            def get = RequestEntity.get(URI.create("http://localhost")).build()
            repository.findOne(_) >> UserDto.builder().email("123@aa.bb").username("me").build();
            headerService.extractToken(_) >> token;
            tokenService.isTokenValid(_) >> true
        when:
            ResponseEntity responseEntity = users.getMe(get)
        then:
            responseEntity.statusCodeValue == 200
            responseEntity.body.username == "me"
            responseEntity.body.email == "123@aa.bb"
    }

    def "should 401 for not found token"() {
        given:
            def get = RequestEntity.get(URI.create("http://localhost")).build()
            repository.findOne(_) >> UserDto.builder().email("123@aa.bb").username("me").build();
            headerService.extractToken(_) >> Optional.empty();
        when:
            ResponseEntity responseEntity = users.getMe(get)
        then:
            responseEntity.statusCodeValue == 401
    }
}