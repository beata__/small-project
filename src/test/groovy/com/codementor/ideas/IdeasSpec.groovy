package com.codementor.ideas

import com.codementor.authentication.UserDto
import com.codementor.authentication.UserRepository
import com.codementor.authentication.token.TokenService
import com.codementor.response.HeaderService
import org.apache.commons.text.RandomStringGenerator
import org.springframework.data.domain.Page
import org.springframework.http.RequestEntity
import spock.lang.Specification
import spock.lang.Unroll

class IdeasSpec extends Specification {

    IdeaRepository ideaRepository = Mock()
    UserRepository userRepository = Mock()
    TokenService tokenService = Mock()
    HeaderService headerService = Spy()
    Ideas ideas = new Ideas(ideaRepository, userRepository, tokenService, headerService)

    @Unroll
    def "Should getIdeas with correct page and token"() {
        given:
            tokenService.isTokenValid(_) >> true
            headerService.extractToken(_) >> Optional.of("1")
            def ideaRequest = RequestEntity.get(URI.create("http://localhost")).build()
            userRepository.findOne(_) >> UserDto.builder().email("a@b.c").build()
            Page page = Mock()
            ideaRepository.findAll(_, _) >> page
            page.getContent() >> [
                    IdeaDto.builder().content("cont").impact(1).ease(2).confidence(3).build(),
                    IdeaDto.builder().content("cont").impact(2).ease(2).confidence(3).build(),
                    IdeaDto.builder().content("cont").impact(4).ease(2).confidence(3).build(),
                    IdeaDto.builder().content("cont").impact(6).ease(2).confidence(3).build()
            ]
        when:
            def ideas = ideas.getIdeas(ideaRequest, pageNumber)
        then:
            ideas.statusCodeValue == 200
            ideas.body.size() == 4
            ideas.body[2].impact == 4
        where:
            pageNumber | _
            1          | _
            1000       | _
            null       | _
    }


    def "Should return bad request when page is equal or less than to zero"() {
        given:
            tokenService.isTokenValid(_) >> true
            headerService.extractToken(_) >> Optional.of("1")
            def ideaRequest = RequestEntity.get(URI.create("http://localhost")).build()
            userRepository.findOne(_) >> UserDto.builder().email("a@b.c").build()
            Page page = Mock()
            ideaRepository.findAll(_, _) >> page
            page.getContent() >> [
                    IdeaDto.builder().content("cont").impact(1).ease(2).confidence(3).build(),
                    IdeaDto.builder().content("cont").impact(2).ease(2).confidence(3).build(),
                    IdeaDto.builder().content("cont").impact(4).ease(2).confidence(3).build(),
                    IdeaDto.builder().content("cont").impact(6).ease(2).confidence(3).build()
            ]
        when:
            def ideas = ideas.getIdeas(ideaRequest, pageNumber)
        then:
            ideas.statusCodeValue == 400
        where:
            pageNumber | _
            0          | _
            -1         | _

    }

    def "should return 200 to store well defined idea"() {
        given:
            tokenService.isTokenValid(_) >> true
            headerService.extractToken(_) >> Optional.of("1")
            def idea = IdeaDto.builder().impact(impact).confidence(confidence).content(content).ease(ease).build()
            def ideaRequest = RequestEntity.post(URI.create("http://localhost")).body(idea)
            userRepository.findOne(_) >> UserDto.builder().email("a@b.c").build()

        when:
            def ideaResponse = ideas.postIdea(ideaRequest);
        then:
            1 * ideaRepository.saveAndFlush(idea) >> idea
            ideaResponse.statusCodeValue == 200
            ideaResponse.body.ease == ease
            ideaResponse.body.impact == impact
            ideaResponse.body.confidence == confidence
            ideaResponse.body.content == content
            ideaResponse.body.averangeScore != null
        where:
            confidence | impact | ease | content
            1          | 1      | 1    | "abc"
    }

    @Unroll
    def "should return 400 for wrongly defined idea"() {
        given:
            tokenService.isTokenValid(_) >> true
            headerService.extractToken(_) >> Optional.of("1")
            def idea = IdeaDto.builder().impact(impact).confidence(confidence).content(content).ease(ease).build()
            def ideaRequest = RequestEntity.post(URI.create("http://localhost")).body(idea)
            userRepository.findOne(_) >> UserDto.builder().email("a@b.c").build()

        when:
            def ideaResponse = ideas.postIdea(ideaRequest);
        then:
            0 * ideaRepository.saveAndFlush(idea)
            ideaResponse.statusCodeValue == 400
            ideaResponse.body.validation != null
        where:
            confidence | impact | ease | content
            1          | null   | 1    | "abc"
            null       | 1      | 1    | "abc"
            1          | 1      | null | "abc"
            1          | 1      | 1    | ""
            1          | 1      | 1    | null
            11         | 1      | 1    | "abc"
            1          | 11     | 1    | "abc"
            1          | 1      | 11   | "abc"
            1          | 1      | 1    | new RandomStringGenerator.Builder().build().generate(256)
    }

    def "should return 200 to update well defined idea"() {
        given:
            tokenService.isTokenValid(_) >> true
            headerService.extractToken(_) >> Optional.of("1")
            def idea = IdeaDto.builder().impact(impact).confidence(confidence).content(content).ease(ease).build()
            def ideaRequest = RequestEntity.post(URI.create("http://localhost")).body(idea)
            userRepository.findOne(_) >> UserDto.builder().email("a@b.c").build()
            def key = "12"
            def dto = new IdeaDto()
            ideaRepository.findOne(key) >> dto
        when:
            def ideaResponse = ideas.updateIdea(ideaRequest, key);
        then:
            1 * ideaRepository.saveAndFlush(dto) >> dto
            ideaResponse.statusCodeValue == 200
            ideaResponse.body.ease == ease
            ideaResponse.body.impact == impact
            ideaResponse.body.confidence == confidence
            ideaResponse.body.content == content
            ideaResponse.body.averangeScore != null
        where:
            confidence | impact | ease | content
            1          | 1      | 1    | "abc"
    }

    @Unroll
    def "should return 400 for wrong update of defined idea"() {
        given:
            tokenService.isTokenValid(_) >> true
            headerService.extractToken(_) >> Optional.of("1")
            def idea = IdeaDto.builder().impact(impact).confidence(confidence).content(content).ease(ease).build()
            def ideaRequest = RequestEntity.put(URI.create("http://localhost")).body(idea)
            userRepository.findOne(_) >> UserDto.builder().email("a@b.c").build()
            def key = "12"
            ideaRepository.findOne(key) >> new IdeaDto()
        when:
            def ideaResponse = ideas.updateIdea(ideaRequest, key);
        then:
            0 * ideaRepository.saveAndFlush(idea)
            ideaResponse.statusCodeValue == 400
            ideaResponse.body.validation != null
        where:
            confidence | impact | ease | content
            1          | null   | 1    | "abc"
            null       | 1      | 1    | "abc"
            1          | 1      | null | "abc"
            1          | 1      | 1    | ""
            1          | 1      | 1    | null
            0          | 1      | 1    | "abc"
            1          | 0      | 1    | "abc"
            1          | 1      | 0    | "abc"
            11         | 1      | 1    | "abc"
            1          | 11     | 1    | "abc"
            1          | 1      | 11   | "abc"
            1          | 1      | 1    | new RandomStringGenerator.Builder().build().generate(256)
    }

    @Unroll
    def "should return 404 for wrong update of not defined"() {
        given:
            tokenService.isTokenValid(_) >> true
            headerService.extractToken(_) >> Optional.of("1")
            def idea = IdeaDto.builder().impact(impact).confidence(confidence).content(content).ease(ease).build()
            def ideaRequest = RequestEntity.put(URI.create("http://localhost")).body(idea)
            userRepository.findOne(_) >> UserDto.builder().email("a@b.c").build()
            def key = "12"
            ideaRepository.findOne(key) >> null
        when:
            def ideaResponse = ideas.updateIdea(ideaRequest, key);
        then:
            0 * ideaRepository.saveAndFlush(idea)
            ideaResponse.statusCodeValue == 404
        where:
            confidence | impact | ease | content
            1          | 1      | 1    | "abc"
    }

    @Unroll
    def "should try to remove idea"() {
        given:
            tokenService.isTokenValid(_) >> true
            headerService.extractToken(_) >> Optional.of("1")
            def ideaRequest = RequestEntity.delete(URI.create("http://localhost")).build()
            userRepository.findOne(_) >> UserDto.builder().email("a@b.c").build()
            String key = "12"
            ideaRepository.findOne(key) >> IdeaDto.builder().user(UserDto.builder().email(email1).build()).build();
            tokenService.extractEmail(_) >> email2
        when:
            def ideaResponse = ideas.deleteIdea(ideaRequest, key);
        then:
            times * ideaRepository.delete(key)
            ideaResponse.statusCodeValue == respCode
        where:
            email1  | email2   || times | respCode
            "a@b.c" | "a@b.c"  || 1     | 200
            "a@b.c" | "ba@b.c" || 0     | 401
    }

    @Unroll
    def "should try to remove idea and have same response each time"() {
        given:
            tokenService.isTokenValid(_) >> true
            headerService.extractToken(_) >> Optional.of("1")
            def ideaRequest = RequestEntity.delete(URI.create("http://localhost")).build()
            userRepository.findOne(_) >> UserDto.builder().email("a@b.c").build()
            String key = "12"
            ideaRepository.findOne(key) >> IdeaDto.builder().user(UserDto.builder().email(email1).build()).build() >> null;
            tokenService.extractEmail(_) >> email2
        when:
            def ideaResponse = ideas.deleteIdea(ideaRequest, key);
        then:
            times * ideaRepository.delete(key)
            ideaResponse.statusCodeValue == respCode
        when:
            def ideaResponse2 = ideas.deleteIdea(ideaRequest, key);
        then:
            0 * ideaRepository.delete(key)
            ideaResponse2.statusCodeValue == respCode
        where:
            email1  | email2  || times | respCode
            "a@b.c" | "a@b.c" || 1     | 200

    }


    def "should return 401 when not active token to remove idea"() {
        given:
            tokenService.isTokenValid(_) >> false
            headerService.extractToken(_) >> Optional.of("1")
            def ideaRequest = RequestEntity.delete(URI.create("http://localhost")).build()
            userRepository.findOne(_) >> UserDto.builder().email("a@b.c").build()
            String key = "12"
        when:
            def ideaResponse = ideas.deleteIdea(ideaRequest, key);
        then:
            0 * ideaRepository.delete(key)
            ideaResponse.statusCodeValue == 401

    }

    def "should return 401 for not valid token when updating well defined idea"() {
        given:
            tokenService.isTokenValid(_) >> false
            headerService.extractToken(_) >> Optional.of("1")
            def idea = IdeaDto.builder().impact(impact).confidence(confidence).content(content).ease(ease).build()
            def ideaRequest = RequestEntity.post(URI.create("http://localhost")).body(idea)
            userRepository.findOne(_) >> UserDto.builder().email("a@b.c").build()
            def key = "12"
            def dto = new IdeaDto()
            ideaRepository.findOne(key) >> dto
        when:
            def ideaResponse = ideas.updateIdea(ideaRequest, key);
        then:
            0 * ideaRepository.saveAndFlush(dto) >> dto
            ideaResponse.statusCodeValue == 401

        where:
            confidence | impact | ease | content
            1          | 1      | 1    | "abc"
    }

    def "should return 401 for not valid token when creating well defined idea"() {
        given:
            tokenService.isTokenValid(_) >> false
            headerService.extractToken(_) >> Optional.of("1")
            def idea = IdeaDto.builder().impact(impact).confidence(confidence).content(content).ease(ease).build()
            def ideaRequest = RequestEntity.post(URI.create("http://localhost")).body(idea)
            userRepository.findOne(_) >> UserDto.builder().email("a@b.c").build()
            def key = "12"
            def dto = new IdeaDto()
            ideaRepository.findOne(key) >> dto
        when:
            def ideaResponse = ideas.postIdea(ideaRequest);
        then:
            0 * ideaRepository.saveAndFlush(dto) >> dto
            ideaResponse.statusCodeValue == 401

        where:
            confidence | impact | ease | content
            1          | 1      | 1    | "abc"
    }

    def "Should  return 401 when token is invalid  forgetIdeas with correct page "() {
        given:
            tokenService.isTokenValid(_) >> false
            headerService.extractToken(_) >> Optional.of("1")
            def ideaRequest = RequestEntity.get(URI.create("http://localhost")).build()
            userRepository.findOne(_) >> UserDto.builder().email("a@b.c").build()
            Page page = Mock()
            ideaRepository.findAll(_, _) >> page
            page.getContent() >> [
                    IdeaDto.builder().content("cont").impact(1).ease(2).confidence(3).build(),
                    IdeaDto.builder().content("cont").impact(2).ease(2).confidence(3).build(),
                    IdeaDto.builder().content("cont").impact(4).ease(2).confidence(3).build(),
                    IdeaDto.builder().content("cont").impact(6).ease(2).confidence(3).build()
            ]
        when:
            def ideas = ideas.getIdeas(ideaRequest, pageNumber)
        then:
            ideas.statusCodeValue == 401
        where:
            pageNumber | _
            1          | _
    }
}
