package com.codementor.response

import org.springframework.http.RequestEntity
import spock.lang.Specification

class HeaderServiceSpec extends Specification {
    HeaderService headerService = new HeaderService();

    def "should ba able to get extract token if exists"() {
        def headerToken = "12"
        given:
            RequestEntity entity = RequestEntity.post(URI.create("http://localhost")).header("x-access-token", headerToken).build();
        when:
            def token = headerService.extractToken(entity);
        then:
            token.isPresent();
            token.get() == headerToken

    }
}
